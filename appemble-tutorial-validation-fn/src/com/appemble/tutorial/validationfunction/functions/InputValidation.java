package com.appemble.tutorial.validationfunction.functions;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.Utilities;
import com.appemble.avm.actions.ALERT;
import com.appemble.avm.actions.CustomFunctionInterface;
import com.appemble.avm.models.ActionModel;

public class InputValidation implements CustomFunctionInterface {

	/**
	 * Retrieves the user input from the screen, validates them 
	 * If the validation fails, an Alert Box is displayed 
	 * and function returns Boolean.valueOf(false) 
	 * If the validation succeeds the function return Boolean.valueOf(true) 
	 *
	 * @param  context It represents more information about an <a href="http://developer.android.com/reference/android/content/Context.html">Activity"</a> (or screen) on which an event was received and this action is being invoked. 
	 * @param  view It is a UI component on which an event was received and this action got triggered. Good reading about <a href="http://developer.android.com/reference/android/view/View.html">View</a>
	 * @param  parentView It is the container in which the control is created. For controls created under screen, title, CONTROL_GROUP this container is of type <a href="http://developer.android.com/reference/android/widget/RelativeLayout.html">Relative Layout</a>. A control can be part of a list item in which the container is the list box item of type CONTROL_GROUP. 
	 * @param  action It has the attributes defining the action itself. 
     * @param  targetParameterValueList The parameter targetParameterValueList is a Bundle. It contains keys defined in the action's attribute input_parameter_list. Note: In case the target needs values with a different name of the key(s), you can define the attribute "target_parameter_list". The target_parameter_list must have the same number of field_name (comma separated) as in the input_parameter_list and MUST be in the same order. 

	 * @author Appemble <a mailto:"support@appemble.com">Support Team</a>
	 * @return Object Boolean.valueOf(true) or Boolean.valueOf(false)
	 */
	@Override
	public Object execute(Context context, View view, View parentView,
		ActionModel action, Bundle targetParameterValueList) {
		
		String sFirstName = null;
		String sLastName = null;
		String sEmail = null;
		String sPassword = null;
		StringBuffer sUIVerificationMessage = new StringBuffer();
		 // User Input Validation
		try {

			// Validate the first name
			sFirstName = targetParameterValueList.getString("firstname");
			if (null != sFirstName)
				sFirstName = sFirstName.trim();
			if(null == sFirstName || 0 == sFirstName.length()) {
				sUIVerificationMessage.append("First Name field cannot be blank. \r\n");
			} else if (sFirstName.length() > 30) {
				sUIVerificationMessage.append("First Name cannot be more than 30 characters. \r\n");
			}

			// Validate the last name
			sLastName = targetParameterValueList.getString("lastname");
			if (null != sLastName)
				sLastName = sLastName.trim();
			if(null == sLastName || 0 == sLastName.length()) {
				sUIVerificationMessage.append("Last Name field cannot be blank. \r\n");
			} else if (sLastName.length() > 30) {
				sUIVerificationMessage.append("Last Name cannot be more than 30 characters. \r\n");
			}

			// Validate the email
			sEmail = targetParameterValueList.getString("email");
			if (null != sEmail)
				sEmail = sEmail.trim();
			if(null == sEmail || 0 == sEmail.length()) {
				sUIVerificationMessage.append("Email cannot be blank. \r\n");
			} else if (Utilities.isEmail(sEmail) == false) {
				sUIVerificationMessage.append("Email is not a valid email address.\r\n");
			}

			// Validate the password
			sPassword = targetParameterValueList.getString("password");
			if (null != sPassword)
				sPassword = sPassword.trim();
			if(null == sPassword || 0 == sPassword.length()) {
				sUIVerificationMessage.append("Password field cannot be blank.\r\n ");
			} else if (sPassword.length() < 8) {
				sUIVerificationMessage.append("Password field cannot be less than 8 chars. \r\n");
			} else {
				boolean upperFound = false;
				for (char c : sPassword.toCharArray()) {
				    if (Character.isUpperCase(c)) {
				        upperFound = true;
				        break;
				    }
				}
			    if (false == upperFound)
			    	sUIVerificationMessage.append("Password does not contain an Upper Case letter. \r\n");
				boolean numberFound = false;
				for (char c : sPassword.toCharArray()) {
				    if (Character.isDigit(c)) {
				        numberFound = true;
				        break;
				    }
				}				    
			    if (false == numberFound)
			    	sUIVerificationMessage.append("Password does not contain a digit. \r\n");
			}
		} 
		catch(Exception e) {
			e.printStackTrace();
		}
		if (sUIVerificationMessage.length() > 0) {
			ALERT.showAlert(context, "Validation Error: ", sUIVerificationMessage.toString());
			return Boolean.valueOf(false);
		}
		
		return Boolean.valueOf(true);
	}

}
